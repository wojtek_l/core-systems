package recruitment.demo.solution.service;


/**
 * Resources' paths.
 */
final class Paths {
    static final String REVIEWS = "reviews";

    private Paths() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }
}
