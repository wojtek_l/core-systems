package recruitment.demo.solution.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import recruitment.demo.solution.model.Review;

/**
 * Repository exposing CRUD operations for {@link Review} entity.
 */
@RepositoryRestResource(path = Paths.REVIEWS)
public interface ReviewRestRepository extends CrudRepository<Review, Long> {
}
