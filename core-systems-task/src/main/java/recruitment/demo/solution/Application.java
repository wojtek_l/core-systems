package recruitment.demo.solution;

import java.util.Arrays;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import recruitment.demo.solution.model.Review;
import recruitment.demo.solution.service.ReviewRestRepository;

/**
 * Bootstrap class to start the app
 */
@SpringBootApplication
public class Application {

    private final static Random RANDOM_GENERATOR = new Random();
    private final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner init(final ReviewRestRepository reviewRestRepository) {
        LOGGER.info("Loading initial test data.");
        return (evt) ->
                Arrays.asList("quickSort", "mergeSort", "binarySearch", "redTrees", "simpleDemo", "jdbcApp")
                        .forEach(taskName -> reviewRestRepository.save(createMockReview(taskName)));
    }

    private Review createMockReview(final String taskName) {
        return new Review("Task about implementing " + taskName, taskName, RANDOM_GENERATOR.nextInt(10));
    }
}