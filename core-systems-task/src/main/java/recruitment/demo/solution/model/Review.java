package recruitment.demo.solution.model;


import javax.persistence.*;
import java.util.Objects;


/**
 * Review entity.
 */
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    private String taskName;
    private String description;
    private Integer score;

    public Review() {
    }

    public Review(String description, String taskName, Integer score) {
        this.description = description;
        this.taskName = taskName;
        this.score = score;
    }


    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }


    @Override
    public String toString() {
        return "Review{" +
                "description='" + description + '\'' +
                ", taskName='" + taskName + '\'' +
                ", score=" + score +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return Objects.equals(taskName, review.taskName) &&
                Objects.equals(description, review.description) &&
                Objects.equals(score, review.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskName, description, score);
    }
}
