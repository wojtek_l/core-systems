package recruitment.demo.solution.service;

import static io.restassured.RestAssured.given;
import static io.restassured.config.DecoderConfig.decoderConfig;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;

import javax.json.Json;
import javax.ws.rs.core.MediaType;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;
import org.junit.Test;


public class ReviewRestRepositoryTest {

    private static final String REVIEW_ID = "/1";
    private static final String REVIEW_ID2 = "/2";
    private static RequestSpecification spec;

    @BeforeClass
    public static void initSpec() {
        RestAssured.config = RestAssured.config().decoderConfig(decoderConfig().defaultContentCharset("UTF-8"));

        spec = new RequestSpecBuilder()
            .setContentType(ContentType.JSON)
            .setBaseUri(TestsConfig.BASE_URL)
            .addFilter(new ResponseLoggingFilter())
            .addFilter(new RequestLoggingFilter())
            .build();
    }

    @Test
    public void shouldUpdateReview() {
        given()
            .spec(spec)
            .body(Json.createObjectBuilder()
                .add("taskName", "weirdName")
                .add("description", "weird description")
                .add("score", 2)
                .build()
                .toString())
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .put(Paths.REVIEWS + REVIEW_ID2)
            .then()
            .statusCode(OK.getStatusCode())
            .body("description", equalTo("weird description"))
            .body("score", equalTo(2))
            .body("taskName", equalTo("weirdName"))
            .body("_links.self.href", equalTo(TestsConfig.BASE_URL + "/" + Paths.REVIEWS + REVIEW_ID2));
    }

    @Test
    public void shouldCreateAndDeleteNewReview() {

        final int initialReviewCount = getReviewCount();

        final String newTaskName = "newTask";
        final String linkToNewResource = given()
            .spec(spec)
            .body(Json.createObjectBuilder()
                .add("taskName", newTaskName)
                .add("description", "fake description")
                .add("score", 5)
                .build()
                .toString())
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .post(Paths.REVIEWS)
            .then()
            .statusCode(CREATED.getStatusCode())
            .body("description", equalTo("fake description"))
            .body("score", equalTo(5))
            .body("taskName", equalTo(newTaskName))
            .body("_links.self.href", notNullValue())
            .extract()
            .path("_links.self.href");

        assertEquals("number of reviews is wrong", initialReviewCount + 1, getReviewCount());

        given()
            .spec(spec)
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .get(linkToNewResource)
            .then()
            .statusCode(OK.getStatusCode())
            .body("taskName", equalTo(newTaskName));

        given()
            .spec(spec)
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .delete(linkToNewResource)
            .then()
            .statusCode(NO_CONTENT.getStatusCode());

        assertEquals("number of reviews is wrong", initialReviewCount, getReviewCount());
    }

    @Test
    public void shouldGetSingleReviews() {
        given()
            .spec(spec)
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .get(Paths.REVIEWS + REVIEW_ID)
            .then()
            .statusCode(OK.getStatusCode())
            .body("description", equalTo("Task about implementing quickSort"))
            .body("score", notNullValue())
            .body("taskName", equalTo("quickSort"))
            .body("_links.self.href", equalTo(TestsConfig.BASE_URL + "/" + Paths.REVIEWS + REVIEW_ID));
    }

    @Test
    public void shouldListAllReviews() {
        given()
            .spec(spec)
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .get(Paths.REVIEWS)
            .then()
            .statusCode(OK.getStatusCode())
            .root("_embedded.reviews")
            .body("size()", greaterThanOrEqualTo(6));
    }

    private int getReviewCount() {
        return given()
            .spec(spec)
            .accept(MediaType.APPLICATION_JSON)
            .when()
            .get(Paths.REVIEWS)
            .then()
            .statusCode(OK.getStatusCode())
            .extract().path("_embedded.reviews.size()");
    }
}